import './css/App.css';
import React from 'react';
import BulletinBoard from './Components/BulletinBoard';
import {ThemeProvider, createTheme} from "@mui/material/styles";
import {CssBaseline} from "@mui/material";

export const ajaxUrl = 'https://dssv-tartaros.nl/wordpress/wp-admin/admin-ajax.php';


function App() {
    const theme = createTheme({
        palette: {
            mode: 'dark',
        }
    })

    console.log(theme.palette.mode)

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <BulletinBoard/>
        </ThemeProvider>
    );
}

export default App;
