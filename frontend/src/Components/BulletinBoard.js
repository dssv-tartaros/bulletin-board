import React, {useEffect} from 'react';
import {ajaxUrl} from "../App";
import * as mockBullets from '../helpers/mockBullets.json';
import {
    Button,
    Paper,
    Table,
    TableBody, TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";

function BulletinBoard() {

    const [bulletPoints, setBulletPoints] = React.useState(null);
    const [claimedBulletPoints, setClaimedBulletPoints] = React.useState([]);
    const [completedBulletPoints, setCompletedBulletPoints] = React.useState([]);
    const [currentUser, setCurrentUser] = React.useState(null);

    useEffect(() => {
        // WordPress call to get the markers from the database. If in development, use mockMarkers.json file
        const data = new FormData();
        data.append('action', 'get_bulletin_board');
        if (process.env.NODE_ENV === 'production') {
            fetch(ajaxUrl, {
                method: 'POST',
                credentials: 'include',
                body: data
            }).then()
                .then(response => response.json())
                .then(data => {
                    console.log(data)
                    setCurrentUser(data.user);
                })
                .catch(error => {
                    console.log('error: ', error)
                })
        } else if (bulletPoints === null) {
            let mockData = mockBullets.default
            setBulletPoints(mockData.bullets);
            setClaimedBulletPoints(mockData.claimedBullets);
            setCurrentUser(mockData.user)
        }
    })

    const setClaimedBulletPoint = (bulletPoint) => {
        bulletPoint.claimer = currentUser;
        setClaimedBulletPoints([...claimedBulletPoints, bulletPoint])
        // remove from bulletPoints
        setBulletPoints(bulletPoints.filter(bullet => bullet.id !== bulletPoint.id))
        console.log(bulletPoint)
    }

    const setCompletedBulletPoint = (bulletPoint) => {
        setCompletedBulletPoints([...completedBulletPoints, bulletPoint])
        // remove from claimedBulletPoints
        setClaimedBulletPoints(claimedBulletPoints.filter(bullet => bullet.id !== bulletPoint.id))
        console.log(bulletPoint)
    }

    // bulletin board in which users can claim tasks

    return (
        <div className="bulletin-board">
            <h1>Bulletin Board</h1>
            <div className="bulletin-board-content">
                <h3>Available Tasks</h3>
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Task</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>By</TableCell>
                                <TableCell>Claim</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {bulletPoints && bulletPoints.map((bulletPoint, index) => {
                                return <TableRow key={index}>
                                    <TableCell>{bulletPoint.title}</TableCell>
                                    <TableCell>{bulletPoint.description}</TableCell>
                                    <TableCell>{bulletPoint.poster}</TableCell>
                                    <TableCell>
                                        <Button
                                            onClick={() => setClaimedBulletPoint(bulletPoint)}
                                            variant={"outlined"}
                                        >Claim</Button>
                                    </TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <h3>Claimed Tasks</h3>
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Task</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>By</TableCell>
                                <TableCell>Complete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {claimedBulletPoints.map((bulletPoint, index) => {
                                return <TableRow key={index}>
                                    <TableCell>{bulletPoint.title}</TableCell>
                                    <TableCell>{bulletPoint.description}</TableCell>
                                    <TableCell>{bulletPoint.claimer}</TableCell>
                                    {currentUser === bulletPoint.claimer && <TableCell>
                                        <Button
                                            onClick={() => setCompletedBulletPoint(bulletPoint)}
                                            variant={"outlined"}
                                        >
                                            Complete</Button>
                                    </TableCell>}
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <h3>Completed Tasks</h3>
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Task</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>Completed By</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {completedBulletPoints.map((bulletPoint, index) => {
                                return <TableRow key={index}>
                                        <TableCell>{bulletPoint.title}</TableCell>
                                        <TableCell>{bulletPoint.description}</TableCell>
                                        <TableCell>{bulletPoint.claimer}</TableCell>
                                </TableRow>
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>
    )
}

export default BulletinBoard;
