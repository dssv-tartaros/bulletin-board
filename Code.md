# Tartaros Stickers React Plugin

This plugin has been written in React.\
It consists of a single React component, which is used to render the map and the stickers on them.\
`OpenLayers` is the library used to render the map and the stickers.\
A sticker is a point in OpenLayers.\
Currently OpenStreetMap is used as the map source, but Google Maps is also supported.

### Database
The current stickers are stored in the Wordpress database.\
This database has 6 fields: `id`, `lat`, `lon`, `sender`, `image` and `distance`.\
The details of this can be found in `react.php`.\
In this file the functions `get_markers`, `delete_marker`, `upload_marker_image` and `add_marker` are used.\
These functions respectively allow a post request to get all current markers, delete a marker based on its id, upload an image to a marker and add a new marker.

### React
The main React component is called `OpenLayer`.\
This component is used to render the map and the markers.\
The map is rendered in a div, below the div is a paragraph which shows the coordinates of last place the user clicked.\
There are two helper files, `MapHelpers.js` for functions related to the map and `MarkerHelpers.js` for functions related to the markers.\
Features are added to the map using the `featuresLayer` variable.\
A custom class CustomPoint has been created to store the distance to the Tartaros track, the sender name and the image location.\
In `MarkerHelpers.js` the style of the markers are defined.\
During initialization the Wordpress database is queried and the markers are added to the map.\
Because Wordpress has CORS is used, there is a file called `mockMarkers.json`.
This file is used to mock the database during development. You can add markers to this file and the map will be updated on creation.\
To smooth local development, the local storage in the browser is used. When you add a marker to the map it is stored there in the same format as the database.
