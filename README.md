# Tartaros Prikbord React Plugin

**Author:** Jelle Buitenhuis\
**Version:** 1.0.0\
**Description:** This is a plugin for our Wordpress website to add a bulletin board.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.\
Uses Craco to consistently output the same filenames. The build files will be placed in `static/js/main.js` or `static/css/main.css`

Your app is ready to be deployed!

## Deployment
To deploy the app to wordpress you'll need to take a couple of steps:
1. First open an ftp client and connect to Wordpress.
2. Navigate to `wordpress/wp-content/plugins/react/frontend`
3. Upload the content of the `build` folder to the `frontend` folder.
4. You're done!
