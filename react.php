<?php
/*
Plugin Name:  Bulletin Board Tartaros
Description:  A plugin where users can create tasks for others to complete
Version:      20221018
Author:       Jelle Buitenhuis
Author URI: https://jellebuitenhuis.nl

*/

// First register resources with init
function bulletin_board_init() {
    $path = "/frontend/static";
    if(getenv('WP_ENV')=="development") {
        $path = "/frontend/build/static";
    }
    wp_register_script("my_react_app_js", plugins_url($path."/js/main.js", __FILE__), array(), "1.0", false);
    wp_register_style("my_react_app_css", plugins_url($path."/css/main.css", __FILE__), array(), "1.0", "all");
}

add_action( 'init', 'bulletin_board_init' );


// Function for the short code that call React app
function tartaros_bulletin_board() {
    wp_enqueue_script("my_react_app_js", '1.0', true);
    wp_enqueue_style("my_react_app_css");
    return "<div id=\"tartaros_react_app\"></div>";
}

add_shortcode('tartaros_bulletin_board', 'tartaros_bulletin_board');
//
// add_action('wp_ajax_add_marker', 'add_marker');
//
// function add_marker()
// {
// 	global $wpdb;
// 	$user_name = wp_get_current_user();
// 	$sender_name = $user_name->display_name;
// 	$lat = round(sanitize_text_field($_POST['lat']), 10);
// 	$lon = round(sanitize_text_field($_POST['lon']), 10);
// 	$distance = sanitize_text_field($_POST['distance']);
// 	$image = sanitize_text_field($_POST['image']);
// 	$image = !empty($image) ? $image : "";
//
// 	$table_name_like = $wpdb->prefix . "stickers_tartaros_content";
//
// 	$sql = $wpdb->prepare( "SELECT COUNT(*) FROM wp_stickers_tartaros_content WHERE lat=%s AND lon=%s", $lat, $lon );
// 	$marker_count = $wpdb->get_var($sql);
// 	$user = wp_get_current_user()->user_login;
// 	if($marker_count <= 0)
// 	{
// 		$result = $wpdb->insert(
// 			$table_name_like,
// 				array(
// 					'sender' => $sender_name,
// 					'lat' => $lat,
// 					'lon' => $lon,
// 					'sender' => $user,
// 					'distance' => $distance,
// 					'image' => $image,
// 				)
// 		);
// 	    echo $wpdb->insert_id;
// 	}
// 	else {
// 	    echo "Marker already exists";
//     }
//
// 	die();
// }
//
// add_action('wp_ajax_get_markers', 'get_markers');
//
// function get_markers()
// {
// 	global $wpdb;
// 	$user_name = wp_get_current_user()->user_login;
//
// 	$sql = $wpdb->prepare ( "SELECT id, lat, lon, sender, distance, image FROM wp_stickers_tartaros_content");
//
//     $markers = $wpdb->get_results ( $sql );
//     $markers_json = json_encode($markers);
//     // replace user
//     $markers_json = str_replace("[{", "{\"coordinates\":[{", $markers_json);
//     $markers_json = str_replace("}]", "}], \"user\":\"".$user_name."\"}", $markers_json);
//
//     echo $markers_json;
//
// 	die();
// }
//
// add_action('wp_ajax_delete_marker', 'delete_marker');
//
// function delete_marker()
// {
// 	global $wpdb;
// 	$user_name = wp_get_current_user()->user_login;
//     $id = $_POST['id'];
//     // delete if sender is the same as the user
//     $sql = $wpdb->prepare ( "DELETE FROM wp_stickers_tartaros_content WHERE id=%s AND sender=%s", $id, $user_name );
//     $result = $wpdb->query($sql);
// 	if($result == 0) {
// 		echo "Not allowed!";
// 	}
//     else {
// 		echo "Marker deleted";
// 	}
//     die();
// }
//
// add_action('wp_ajax_upload_marker_image', 'upload_marker_image');
//
// function upload_marker_image()
// {	if (isset($_FILES['sticker_image'] ) && !empty($_FILES['sticker_image']['name']) )
//         {
//             $allowedExts = array("jpg", "jpeg", "png", "gif", "bmp");
//             $temp = explode(".", $_FILES["sticker_image"]["name"]);
//             $extension = end($temp);
//
//             if ( in_array($extension, $allowedExts))
//             {
//                 if ( ($_FILES["sticker_image"]["error"] > 0) && ($_FILES['sticker_image']['size'] <= 3145728 ))
//                 {
//                     $response = array(
//                         "status" => 'error',
//                         "message" => 'ERROR Return Code: '. $_FILES["sticker_image"]["error"],
//                         );
//                 }
//                 else
//                 {
//                     $uploadedfile = $_FILES['sticker_image'];
//                     $upload_name = $_FILES['sticker_image']['name'];
//                     $uploads = wp_upload_dir();
//                     $filepath = $uploads['path']."/$upload_name";
//
//                     if ( ! function_exists( 'wp_handle_upload' ) )
//                     {
//                         require_once( ABSPATH . 'wp-admin/includes/file.php' );
//                     }
//                     require_once( ABSPATH . 'wp-admin/includes/image.php' );
//                     $upload_overrides = array( 'test_form' => false );
//                     $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
//
//
//                     $response = array(
//                         "status" => 'success',
//                         "url" => $movefile['url']
//                         );
//
//                 }
//             }
//             else
//             {
//                 $response = array(
//                     "status" => 'error',
//                     "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
//                     );
//             }
//         }
//         echo json_encode($response);
// 		die();
// }

/*On plugin activation, run these functions*/
register_activation_hook( __FILE__, 'bulletin_table_install' );

/*The creation of the database for the quotes*/
function bulletin_table_install()
{
    global $wpdb;

    $table_name_content = $wpdb->prefix . "stickers_tartaros_content";

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $table_name_content (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		lat decimal(65,10) NOT NULL,
		lon decimal(65,10) NOT NULL,
		sender tinytext NOT NULL,
		image tinytext,
		distance decimal(65,10) NOT NULL,
		PRIMARY KEY  (id)
		) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
// 	dbDelta( $sql );

}

